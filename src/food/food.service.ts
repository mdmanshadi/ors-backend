import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Food } from '../entities/Food.entity';

@Injectable()
export class FoodService extends TypeOrmCrudService<Food> {
  constructor(@InjectRepository(Food) repo) {
    super(repo);
  }
}
