import { Controller } from '@nestjs/common';
import { FoodService } from './food.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { Food } from '../entities/Food.entity';

@Crud({
  model: {
    type: Food,
  },
})
@Controller('foods')
export class FoodController implements CrudController<Food> {
  constructor(public service: FoodService) {}
}
