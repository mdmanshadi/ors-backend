import { Module } from '@nestjs/common';
import { PaymentService } from './Payment.service';
import { PaymentController } from './Payment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Payment } from '../entities/Payment.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Payment])],
  controllers: [PaymentController],
  providers: [PaymentService],
  exports: [PaymentService],
})
export class PaymentModule {}
