import { Controller } from '@nestjs/common';
import { PaymentService } from './Payment.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { Payment } from '../entities/Payment.entity';

@Crud({
  model: {
    type: Payment,
  },
  query: {
    sort: [
      {
        field: 'id',
        order: 'DESC',
      },
    ],
  },
})
@Controller('Payments')
export class PaymentController implements CrudController<Payment> {
  constructor(public service: PaymentService) {}
}
