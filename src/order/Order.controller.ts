import { Controller } from '@nestjs/common';
import { OrderService } from './Order.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { Order } from '../entities/Order.entity';

@Crud({
  model: {
    type: Order,
  },
  query: {
    sort: [
      {
        field: 'id',
        order: 'DESC',
      },
    ],
  },
})
@Controller('Orders')
export class OrderController implements CrudController<Order> {
  constructor(public service: OrderService) {}
}
