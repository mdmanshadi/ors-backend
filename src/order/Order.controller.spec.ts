import { Test, TestingModule } from '@nestjs/testing';
import { OrderController } from './Order.controller';
import { OrderService } from './Order.service';

describe('UserController', () => {
  let controller: OrderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderController],
      providers: [OrderService],
    }).compile();

    controller = module.get<OrderController>(OrderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
