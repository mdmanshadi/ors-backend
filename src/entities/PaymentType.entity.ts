import { Column, Entity, OneToMany } from 'typeorm';
import { Payment } from './Payment.entity';

@Entity('payment_type', { schema: 'ors' })
export class PaymentType {
  @Column('int', { primary: true, name: 'id' })
  id: number;

  @Column('varchar', { name: 'name', nullable: true, length: 255 })
  name: string | null;

  @Column('text', { name: 'description', nullable: true })
  description: string | null;

  @OneToMany(() => Payment, (payment) => payment.paymentType)
  payments: Payment[];
}
