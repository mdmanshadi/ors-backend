import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { FoodCategory } from './FoodCategory.entity';
import { FoodPicture } from './FoodPicture.entity';
import { OrderFoods } from './OrderFoods.entity';

@Entity('food', { schema: 'ors' })
export class Food {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'name', nullable: true, length: 255 })
  name: string | null;

  @Column('int', { name: 'price', nullable: true })
  price: number | null;

  @Column('int', { name: 'specialPrice', nullable: true })
  specialPrice: number | null;

  @Column('int', { name: 'maxOrderQuantity', nullable: true })
  maxOrderQuantity: number | null;

  @Column('varchar', { name: 'primaryPic', nullable: true, length: 255 })
  primaryPic: string | null;

  @Column('tinyint', { name: 'isActive', nullable: true, default: () => "'1'" })
  isActive: number | null;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToMany(() => FoodCategory, (foodCategory) => foodCategory.foods)
  foodCategories: FoodCategory[];

  @OneToMany(() => FoodPicture, (foodPicture) => foodPicture.food)
  foodPictures: FoodPicture[];

  @OneToMany(() => OrderFoods, (orderFoods) => orderFoods.food)
  orderFoods: OrderFoods[];
}
