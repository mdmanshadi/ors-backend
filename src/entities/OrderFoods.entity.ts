import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Food } from './Food.entity';
import { Order } from './Order.entity';

@Index('fk_order_foods_food1_idx', ['foodId'], {})
@Index('fk_order_foods_order1_idx', ['orderId'], {})
@Entity('order_foods', { schema: 'ors' })
export class OrderFoods {
  @Column('int', { primary: true, name: 'id' })
  id: number;

  @Column('int', { name: 'foodId' })
  foodId: number;

  @Column('int', { name: 'orderId' })
  orderId: number;

  @Column('int', { name: 'foodQuantity', nullable: true })
  foodQuantity: number | null;

  @ManyToOne(() => Food, (food) => food.orderFoods, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'foodId', referencedColumnName: 'id' }])
  food: Food;

  @ManyToOne(() => Order, (order) => order.orderFoods, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'orderId', referencedColumnName: 'id' }])
  order: Order;
}
