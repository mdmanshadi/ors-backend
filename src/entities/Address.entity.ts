import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { State } from './State.entity';
import { User } from './User.entity';
import { OrderDetails } from './OrderDetails.entity';

@Index('fk_address_user1_idx', ['userId'], {})
@Index('fk_address_state1_idx', ['stateId'], {})
@Entity('address', { schema: 'ors' })
export class Address {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'name', nullable: true, length: 255 })
  name: string | null;

  @Column('text', { name: 'fullAddress', nullable: true })
  fullAddress: string | null;

  @Column('int', { name: 'userId' })
  userId: number;

  @Column('int', { name: 'stateId' })
  stateId: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(() => State, (state) => state.addresses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'stateId', referencedColumnName: 'id' }])
  state: State;

  @ManyToOne(() => User, (user) => user.addresses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'userId', referencedColumnName: 'id' }])
  user: User;

  @OneToMany(() => OrderDetails, (orderDetails) => orderDetails.address)
  orderDetails: OrderDetails[];
}
