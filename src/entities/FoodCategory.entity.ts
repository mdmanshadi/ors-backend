import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Food } from './Food.entity';

@Entity('food_category', { schema: 'ors' })
export class FoodCategory {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'name', nullable: true, length: 255 })
  name: string | null;

  @Column('varchar', { name: 'icon', nullable: true, length: 255 })
  icon: string | null;

  @ManyToMany(() => Food, (food) => food.foodCategories)
  @JoinTable({
    name: 'food_has_category',
    joinColumns: [{ name: 'categoryId', referencedColumnName: 'id' }],
    inverseJoinColumns: [{ name: 'foodId', referencedColumnName: 'id' }],
    schema: 'ors',
  })
  foods: Food[];
}
