import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderDetails } from './OrderDetails.entity';
import { PaymentType } from './PaymentType.entity';
import { User } from './User.entity';

@Index('fk_payment_user1_idx', ['userId'], {})
@Index('fk_payment_payment_type1_idx', ['paymentTypeId'], {})
@Entity('payment', { schema: 'ors' })
export class Payment {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('int', { name: 'userId' })
  userId: number;

  @Column('int', { name: 'paymentTypeId' })
  paymentTypeId: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @OneToMany(() => OrderDetails, (orderDetails) => orderDetails.payment)
  orderDetails: OrderDetails[];

  @ManyToOne(() => PaymentType, (paymentType) => paymentType.payments, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'paymentTypeId', referencedColumnName: 'id' }])
  paymentType: PaymentType;

  @ManyToOne(() => User, (user) => user.payments, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'userId', referencedColumnName: 'id' }])
  user: User;
}
