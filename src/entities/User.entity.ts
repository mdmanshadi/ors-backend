import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Address } from './Address.entity';
import { OrderDetails } from './OrderDetails.entity';
import { Payment } from './Payment.entity';

@Index('phone_UNIQUE', ['phone'], { unique: true })
@Entity('user', { schema: 'ors' })
export class User {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'firstName', nullable: true, length: 255 })
  firstName: string | null;

  @Column('varchar', { name: 'lastName', nullable: true, length: 255 })
  lastName: string | null;

  @Column('varchar', {
    name: 'phone',
    nullable: true,
    unique: true,
    length: 11,
  })
  phone: string | null;

  @Column('varchar', { name: 'profileImage', nullable: true, length: 255 })
  profileImage: string | null;

  @Column('varchar', { name: 'sex', nullable: true, length: 16 })
  sex: string | null;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @OneToMany(() => Address, (address) => address.user)
  addresses: Address[];

  @OneToMany(() => OrderDetails, (orderDetails) => orderDetails.user)
  orderDetails: OrderDetails[];

  @OneToMany(() => Payment, (payment) => payment.user)
  payments: Payment[];
}
