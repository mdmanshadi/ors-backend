import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Address } from './Address.entity';

@Entity('state', { schema: 'ors' })
export class State {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'name', nullable: true, length: 255 })
  name: string | null;

  @OneToMany(() => Address, (address) => address.state)
  addresses: Address[];
}
