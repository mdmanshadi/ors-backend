import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  UpdateDateColumn,
} from 'typeorm';
import { Address } from './Address.entity';
import { Payment } from './Payment.entity';
import { User } from './User.entity';
import { Order } from './Order.entity';

@Index('fk_order_details_user1_idx', ['userId'], {})
@Index('fk_order_details_address1_idx', ['addressId'], {})
@Index('fk_order_details_payment1_idx', ['paymentId'], {})
@Entity('order_details', { schema: 'ors' })
export class OrderDetails {
  @Column('int', { primary: true, name: 'id' })
  id: number;

  @Column('varchar', { name: 'status', nullable: true, length: 45 })
  status: string | null;

  @Column('int', { name: 'price', nullable: true })
  price: number | null;

  @Column('int', { name: 'specialPrice', nullable: true })
  specialPrice: number | null;

  @Column('int', { name: 'userId' })
  userId: number;

  @Column('int', { name: 'addressId' })
  addressId: number;

  @Column('int', { name: 'paymentId' })
  paymentId: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(() => Address, (address) => address.orderDetails, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'addressId', referencedColumnName: 'id' }])
  address: Address;

  @ManyToOne(() => Payment, (payment) => payment.orderDetails, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'paymentId', referencedColumnName: 'id' }])
  payment: Payment;

  @ManyToOne(() => User, (user) => user.orderDetails, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'userId', referencedColumnName: 'id' }])
  user: User;

  @OneToMany(() => Order, (order) => order.orderDetails)
  orders: Order[];
}
