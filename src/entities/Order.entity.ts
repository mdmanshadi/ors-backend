import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderFoods } from './OrderFoods.entity';
import { OrderDetails } from './OrderDetails.entity';

@Index('fk_order_order_details1_idx', ['orderDetailsId'], {})
@Entity('order', { schema: 'ors' })
export class Order {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'ordercol', nullable: true, length: 45 })
  ordercol: string | null;

  @Column('int', { name: 'orderDetailsId' })
  orderDetailsId: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @OneToMany(() => OrderFoods, (orderFoods) => orderFoods.order)
  orderFoods: OrderFoods[];

  @ManyToOne(() => OrderDetails, (orderDetails) => orderDetails.orders, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'orderDetailsId', referencedColumnName: 'id' }])
  orderDetails: OrderDetails;
}
