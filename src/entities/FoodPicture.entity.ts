import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Food } from './Food.entity';

@Index('fk_food_picture_food1_idx', ['foodId'], {})
@Entity('food_picture', { schema: 'ors' })
export class FoodPicture {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'url', nullable: true, length: 255 })
  url: string | null;

  @Column('varchar', { name: 'alt', nullable: true, length: 255 })
  alt: string | null;

  @Column('int', { name: 'foodId' })
  foodId: number;

  @ManyToOne(() => Food, (food) => food.foodPictures, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'foodId', referencedColumnName: 'id' }])
  food: Food;
}
