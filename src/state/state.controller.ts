import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { State } from '../entities/State.entity';
import { StateService } from './state.service';

@Crud({
  model: {
    type: State,
  },
  query: {
    sort: [
      {
        field: 'id',
        order: 'DESC',
      },
    ],
  },
})
@Controller('states')
export class StateController implements CrudController<State> {
  constructor(public service: StateService) {}
}
