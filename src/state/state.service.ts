import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { State } from '../entities/State.entity';

@Injectable()
export class StateService extends TypeOrmCrudService<State> {
  constructor(@InjectRepository(State) repo) {
    super(repo);
  }
}
