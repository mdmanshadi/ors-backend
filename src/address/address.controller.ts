import { Controller } from '@nestjs/common';
import { AddressService } from './address.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { Address } from '../entities/Address.entity';

@Crud({
  model: {
    type: Address,
  },
})
@Controller('addresses')
export class AddressController implements CrudController<Address> {
  constructor(public service: AddressService) {}
}
