import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AddressModule } from './address/address.module';
import { StateModule } from './state/state.module';
import { ConfigModule } from '@nestjs/config';
import { FoodModule } from './food/food.module';
import { FoodCategoryModule } from './food-category/food-category.module';
import { OrderModule } from './order/Order.module';
import { PaymentModule } from './payment/payment.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.development', '.env'],
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(),
    UserModule,
    AddressModule,
    StateModule,
    FoodModule,
    FoodCategoryModule,
    OrderModule,
    PaymentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
