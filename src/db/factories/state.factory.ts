import Faker from 'faker';
import { define } from 'typeorm-seeding';
import { State } from '../../entities/State.entity';

define(State, (faker: typeof Faker) => {
  const state = new State();
  state.name = faker.address.city();
  return state;
});
