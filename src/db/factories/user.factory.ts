import Faker from 'faker';
import { define } from 'typeorm-seeding';
import { User } from '../../entities/User.entity';

define(User, (faker: typeof Faker) => {
  const user = new User();
  user.firstName = faker.name.firstName();
  user.lastName = faker.name.lastName();
  user.phone = faker.phone.phoneNumber();
  user.profileImage = faker.image.avatar();
  const sexArray = ['مرد', 'زن'];
  user.sex = faker.random.arrayElement(sexArray);
  return user;
});
