import { Factory, Seeder } from 'typeorm-seeding';
import { State } from '../../entities/State.entity';

export default class CreateStatesSeed implements Seeder {
  run(factory: Factory): Promise<any> {
    return factory(State)().createMany(10);
  }
}
