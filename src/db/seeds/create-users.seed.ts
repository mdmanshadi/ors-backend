import { Factory, Seeder } from 'typeorm-seeding';
import { User } from '../../entities/User.entity';

export default class CreateUsersSeed implements Seeder {
  run(factory: Factory): Promise<any> {
    return factory(User)().createMany(50);
  }
}
