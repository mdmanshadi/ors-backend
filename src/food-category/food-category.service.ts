import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodCategory } from '../entities/FoodCategory.entity';

@Injectable()
export class FoodCategoryService extends TypeOrmCrudService<FoodCategory> {
  constructor(@InjectRepository(FoodCategory) repo) {
    super(repo);
  }
}
