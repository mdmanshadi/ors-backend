import { Controller } from '@nestjs/common';
import { FoodCategoryService } from './food-category.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { FoodCategory } from '../entities/FoodCategory.entity';

@Crud({
  model: {
    type: FoodCategory,
  },
})
@Controller('FoodCategorys')
export class FoodCategoryController implements CrudController<FoodCategory> {
  constructor(public service: FoodCategoryService) {}
}
